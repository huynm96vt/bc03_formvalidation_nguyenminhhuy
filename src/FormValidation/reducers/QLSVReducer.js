import { THEM_SINH_VIEN } from "../constants/constants";

let initialState = {
  listSV: [],
};

export const QLSVReducer = (state = initialState, action) => {
  switch (action.type) {
    case THEM_SINH_VIEN: {
      let listSVUpdated = [...state.listSV, action.payload];
      state.listSV = listSVUpdated;
      return { ...state };
    }
    default:
      return state;
  }
};
