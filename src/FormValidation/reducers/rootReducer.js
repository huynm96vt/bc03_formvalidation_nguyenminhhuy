import { combineReducers } from "redux";
import { QLSVReducer } from "./QLSVReducer";

export const rootReducer = combineReducers({ QLSVReducer });
