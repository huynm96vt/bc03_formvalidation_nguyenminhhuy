import React, { Component } from "react";
import { connect } from "react-redux";
import { THEM_SINH_VIEN } from "./constants/constants";

class FormSV extends Component {
  state = {
    values: {
      maSV: "",
      hoTen: "",
      email: "",
      sdt: "",
    },
    errors: {
      maSV: "",
      hoTen: "",
      email: "",
      sdt: "",
    },
    valid: false,
  };

  handleChange = (e) => {
    let tagInput = e.target;
    // let name = tagInput.name;
    // let value = tagInput.value;
    let { name, value, type, pattern } = tagInput;
    let errorMessage = "";
    if (value.trim() === "") {
      errorMessage = name + ` khong duoc de trong`;
    }
    if (type === "email") {
      const regex = new RegExp(pattern);
      if (!regex.test(value)) {
        errorMessage = name + ` khong duoc de trong`;
      }
    }
    if (name === "sdt") {
      const regex = new RegExp(pattern);
      if (!regex.test(value)) {
        errorMessage = name + ` khong duoc de trong`;
      }
    }

    this.setState(
      {
        values: { ...this.state.values, [name]: value },
        errors: { ...this.state.errors, [name]: errorMessage },
      },
      () => {
        this.isEnableSubmit();
      }
    );
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.themSinhVien(this.state);
  };

  isEnableSubmit = () => {
    let isValid = true;
    for (let key in this.state.errors) {
      if (this.state.errors[key] !== "" || this.state.values[key] === "") {
        isValid = false;
      }
    }

    this.setState({ ...this.state, valid: isValid });
  };
  render() {
    return (
      <div className="container">
        <div className="card">
          <div className="card-header bg-dark text-white">
            <h3>Thông tin sinh viên</h3>
          </div>
          <div className="card-body">
            <form onSubmit={this.handleSubmit}>
              <div className="row">
                <div className="form-group col-6">
                  <label htmlFor>Mã sinh viên</label>
                  <input
                    type="text"
                    name="maSV"
                    id
                    className="form-control"
                    placeholder
                    aria-describedby="helpId"
                    value={this.state.values.maSV}
                    onChange={this.handleChange}
                  />
                  <span className="text-danger">{this.state.errors.maSV}</span>
                </div>
                <div className="form-group col-6">
                  <label htmlFor>Họ tên</label>
                  <input
                    type="text"
                    name="hoTen"
                    id
                    className="form-control"
                    placeholder
                    aria-describedby="helpId"
                    value={this.state.values.hoTen}
                    onChange={this.handleChange}
                  />
                  <span className="text-danger">{this.state.errors.hoTen}</span>
                </div>
                <div className="form-group col-6">
                  <label htmlFor>Số điện thoại</label>
                  <input
                    pattern="^(0|[1-9][0-9]*)$"
                    type="text"
                    name="sdt"
                    id
                    className="form-control"
                    placeholder
                    aria-describedby="helpId"
                    value={this.state.values.sdt}
                    onChange={this.handleChange}
                  />
                  <span className="text-danger">{this.state.errors.sdt}</span>
                </div>
                <div className="form-group col-6">
                  <label htmlFor>Email</label>
                  <input
                    pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
                    type="email"
                    name="email"
                    id
                    className="form-control"
                    placeholder
                    aria-describedby="helpId"
                    value={this.state.values.email}
                    onChange={this.handleChange}
                  />
                  <span className="text-danger">{this.state.errors.email}</span>
                </div>
              </div>
              <div className="row">
                <div className="col-12 text-right">
                  {this.state.valid ? (
                    <button type="submit" className="btn btn-success">
                      Thêm sinh viên
                    </button>
                  ) : (
                    <button type="submit" className="btn btn-success" disabled>
                      Thêm sinh viên
                    </button>
                  )}
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    themSinhVien: (sinhVien) => {
      const action = {
        type: THEM_SINH_VIEN,
        payload: sinhVien,
      };
      dispatch(action);
    },
  };
};

export default connect(null, mapDispatchToProps)(FormSV);
