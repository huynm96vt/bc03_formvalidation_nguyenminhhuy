import React, { Component } from "react";
import { connect } from "react-redux";

class TableSV extends Component {
  renderListSV = () => {
    const listSV = this.props.listSV;
    return listSV.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.values.maSV}</td>
          <td>{item.values.hoTen}</td>
          <td>{item.values.sdt}</td>
          <td>{item.values.email}</td>
        </tr>
      );
    });
  };

  render() {
    return (
      <div className="container">
        <table className="table">
          <thead>
            <tr className="bg-dark text-white">
              <th>Mã sinh viên</th>
              <th>Họ tên</th>
              <th>Số điện thoại</th>
              <th>Email</th>
            </tr>
          </thead>
          <tbody>{this.renderListSV()}</tbody>
        </table>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    listSV: state.QLSVReducer.listSV,
  };
};

export default connect(mapStateToProps)(TableSV);
